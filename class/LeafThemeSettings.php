<?php
/**
 *	Handle Leafstudios Media Theme settings.
 */
class LeafThemeSettings
{
	// Define allowed files
	private $allowed_files;
	// Custom message, to show information for user.
	private $message;
	
	/**
	 * Initialize basic settings for class.
	 */
	public function __construct() {
		$this->allowed_files = array(
			'leaf_header' => array('title' => 'Header bild'),
			'leaf_favicon' => array('title' => 'Favicon'),
		);
		apply_filters('leaf_allowed_files', &$this->allowed_files);
		$this->message = '';
	}
	
	/**
	 *	Add Theme Settings page to admin menu.
	 */
	public function add_admin_page_to_menu() {
		add_theme_page('Leafstudios Media ' . __('Settings'), __('Theme') . ' ' . __('Settings'),
			'edit_theme_options', 'leaf-media-settings', array($this, 'load_admin_page_template'));
	}
	
	/**
	 *	Load template for theme settings page.
	 */
	public function load_admin_page_template() {
		$message = $this->message;
		
		// Set up images
		$images = array();
		foreach($this->allowed_files as $key => $value) {
			$images[] = array(
				'id' => $key,
				'title' => $value['title'],
				'data' => get_option($key),
			);
		}
		
		require(TEMPLATEPATH . '/admin/theme_settings.php');
	}
	
	/**
	 *	Saves upload data from theme settings page.
	 *
	 *	@param array $data
	 *		Should be post data.
	 */
	public function upload($data) {
		$message = array();
		
		// Check if any images should be removed
		foreach($data as $key => $value) {
			if ($value == 'delete') {
				if ($this->delete($key))
					$message[] = sprintf('%s togs bort utan problem.', $this->allowed_files[$key]['title']);
				else
					$message[] = sprintf('%s kunde inte tas bort.', $this->allowed_files[$key]['title']);
			}
		}
		
		// Upload files.
		foreach($this->allowed_files as $key => $value) {
			if (!empty($_FILES[$key]['name'])) {
				if ($this->upload_image($_FILES[$key], $key))
					$message[] = sprintf('%s laddades upp utan problem.', $value['title']);
				else
					$message[] = sprintf('%s kunde inte laddas upp.', $value['title']);
			}
		}
		
		$this->message = implode("<br />\n", $message);
	}
	
	/**
	 *	Upload new image to storage.
	 *
	 *	@param array $files
	 *		Data from $_FILES
	 *	@param string $id
	 *		Identity for file. For future development.
	 *	@return boolean True on success, false on fail.
	 */
	protected function upload_image($files, $id) {
		$upload_dir = wp_upload_dir();
		
		if (!$this->security_check_image($files['tmp_name']))
			return FALSE;
		
		if (get_option($id))
			$this->remove_old_image(get_option($id));
		
		/** Ignore this for now..
		if ($id == 'leaf-header' && $width > 900)
			$this->image_change_size($files['tmp_name'], 900);
		*/
		
		if (!move_uploaded_file($files['tmp_name'], $upload_dir['basedir'] . '/' . $files['name']))
			return FALSE;
		$data = array('url' => $upload_dir['baseurl'] . '/' . $files['name'],
			'path' => $upload_dir['basedir'] . '/' . $files['name']);
		
		return update_option($id, $data);
	}
	
	/**
	 *	Check if image really is an image.
	 *
	 *	@param string $path
	 *		Absolute path to image that we want to check.
	 *	@return boolean/array
	 *		FALSE on fail, or width and height on success array('width' => '', 'height' => '');
	 */
	protected function security_check_image($path) {
		if (!list($width, $height) = getimagesize($path))
			return FALSE;
		else
			return array('width' => $width, 'height' => $height);
	}
	
	/* Ignore this for now...
	protected function image_change_size($filepath, $width) {
		$image = new Imagick($filepath);
		$im
	}
	*/
	
	/**
	 * Remove existing image, and database reference.
	 *
	 * @param string $id
	 *   ID for image
	 * @return boolean
	 *   TRUE for success, FALSE for failure
	 */
	protected function delete($id) {
		if (!$image = get_option($id))
			return FALSE;
		
		if (!$this->remove_old_image($image))
			return FALSE;
		
		return delete_option($id);
	}
	
	/**
	 *	Remove old image from storage.
	 *
	 *	@param array $data
	 *		path => Absolute path to file.
	 *	@return boolean TRUE if file removed, false if not removed.
	 */
	protected function remove_old_image($data) {
		if (file_exists($data['path']))
			return unlink($data['path']);
		else
			return FALSE;
	}
}
Leafstudios Basic Theme

Leafstudios Basic Theme is a very basic Wordpress theme.

This is all about functionality. There should be close to
no styling at all, perhaps only defining a flexible
grid system to make mobile versions possible.

The general idea is to create a child theme, and there you put all
your great stuff in order to create a flashy wonderful theme.
Never do changes to base theme!

Please feel free to contact Leafstudios (info@leafstudios.se) if
you want to use this base theme for your own Wordpress website.
